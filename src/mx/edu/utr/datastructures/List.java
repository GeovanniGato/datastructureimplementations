package mx.edu.utr.datastructures;

/**
 * An ordered collection (also known as a sequence).
 *
 * @author Alejandro Herrera
 */
public interface List {

    /**
     * Appends the specified element to the end of this list.
     *
     * @param o element to be appended to this list.
     * @return <tt>true</tt> if this collection changed as a result of the call.
     */
    public boolean add(Object o);

    /**
     * Inserts the specified element at the specified position in this list.
     *
     * @param index index at which the specified element is to be inserted.
     * @param element element to be inserted.
     */
    public void add(int index, Object element);

    /**
     * Removes all of the elements from this list. *
     */
    public void clear();

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return.
     * @return the element at the specified position in this list.
     */
    public Object get(int index);

    /**
     * Returns the index in this list of the first occurrence of the specified
     * element, or -1 if this list does not contain this element.
     *
     * @param o element to search for.
     * @return the index in this list of the first occurrence of the specified
     * element, or -1 if this list does not contain this element.
     */
    public int indexOf(Object o);

    /**
     * Returns <tt>true</tt> if this list contains no elements.}
     *
     * @return <tt>true</tt> if this list contains no elements.
     */
    public boolean isEmpty();

    /**
     * Removes the element at the specified position in this list.
     *
     * @param index the index of the element to removed.
     * @return the element previously at the specified position.
     */
    public Object remove(int index);

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param index index of element to replace.
     * @param element element to be stored at the specified position.
     * @return the element previously at the specified position.
     */
    public Object set(int index, Object element);

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.list
     */
    public int size();
}
